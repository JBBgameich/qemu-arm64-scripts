#!/usr/bin/env bash

VMLINUZ="vmlinuz-4.9.0-8-arm64"
INITRD="initrd.img-4.9.0-8-arm64"
DISK="disk.qcow2"

virt-copy-out -a ${DISK} /boot/${VMLINUZ} /boot/${INITRD} .

qemu-system-aarch64 -smp 2 -M virt -cpu cortex-a57 -m 3G \
    -initrd ${INITRD} \
    -kernel ${VMLINUZ} \
    -append "root=/dev/sda2 console=ttyAMA0" \
    -global virtio-blk-device.scsi=off \
    -device virtio-scsi-device,id=scsi \
    -drive file=${DISK},id=rootimg,cache=unsafe,if=none \
    -device scsi-hd,drive=rootimg \
    -device e1000,netdev=net0 \
    -net nic \
    -netdev user,hostfwd=tcp::10022-:22,id=net0 \
    -nographic
