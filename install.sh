#!/usr/bin/env bash

DEBIAN_RELEASE="stretch"
DISK_SIZE="20G"

wget --continue \
	http://ftp.debian.org/debian/dists/${DEBIAN_RELEASE}/main/installer-arm64/current/images/netboot/debian-installer/arm64/initrd.gz \
        http://ftp.debian.org/debian/dists/${DEBIAN_RELEASE}/main/installer-arm64/current/images/netboot/debian-installer/arm64/linux

qemu-img create -f qcow2 disk.qcow2 ${DISK_SIZE}

qemu-system-aarch64 -smp 2 -M virt -cpu cortex-a57 -m 1G \
    -initrd initrd.gz \
    -kernel linux -append "root=/dev/ram console=ttyAMA0" \
    -global virtio-blk-device.scsi=off \
    -device virtio-scsi-device,id=scsi \
    -drive file=disk.qcow2,id=rootimg,cache=unsafe,if=none \
    -device scsi-hd,drive=rootimg \
    -netdev user,id=unet -device virtio-net-device,netdev=unet \
    -net user \
    -nographic
